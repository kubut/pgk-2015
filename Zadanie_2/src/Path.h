#ifndef TUTORIALS_PATH_H
#define TUTORIALS_PATH_H
#include <GL/glew.h>
#include <glfw3.h>
#include <glm/glm.hpp>
#include <vector>
#include <common/shader.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Camera.h"


class Path {
public:
    Path(std::vector<GLfloat> vertexes, std::vector<glm::vec3> tracksInfo);
    void setBuffers(bool mode3D);
    void setShaders();
    void setCamera(Camera* camera);
    void draw(bool mode3D);
    ~Path();

private:
    GLuint VertexArrayID, vertexbuffer, pId;
    GLint track_uniform, cameraUniform, matrixAUniform, matrixBUniform, matrixGUniform;
    std::vector<GLfloat> vertexes;
    std::vector<glm::vec3> tracksInfo;
};


#endif //TUTORIALS_PATH_H
