#include "Map.h"
#include <glm/gtc/type_ptr.hpp>

Map::Map(char* file, char* map) {
    img = MapImage(file, map);
}

void Map::setShaders() {
    pId = LoadShaders("map.vertexshader", "map.fragmentshader");
    textureUniform = glGetUniformLocation(pId, "texunit");
    cameraUniform = glGetUniformLocation(pId, "camera");
    matrixAUniform = glGetUniformLocation(pId, "matrixA");
    matrixBUniform = glGetUniformLocation(pId, "matrixB");
    matrixGUniform = glGetUniformLocation(pId, "matrixG");
}

void Map::setBuffers() {
    glUseProgram(pId);

    float* bb = img.getBB();

    GLfloat vert[][2] = {
            { bb[0],  bb[1] },
            { bb[2],  bb[1] },
            { bb[2],  bb[3] },
            { bb[0],  bb[3] },
            { 0., 1. },
            { 1., 1. },
            { 1., 0. },
            { 0., 0. }
    };
    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);
    glGenBuffers(1, &vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);

    glBufferData(GL_ARRAY_BUFFER, 8*2*sizeof(float), vert, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(
            0,                 // attribute 0, must match the layout in the shader.
            2,                  // size
            GL_FLOAT,           // type
            GL_FALSE,           // normalized?
            0,//24,             // stride
            (void*)0            // array buffer offset
    );
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(
            1,                 // attribute 1, but must match the layout in the shader.
            2,                  // size
            GL_FLOAT,           // type
            GL_FALSE,           // normalized?
            0,//24,             // stride
            (void*)(4*2*sizeof(float))          // array buffer offset of Texture Coords
    );
}

void Map::setTexture(){
    glEnable( GL_TEXTURE_2D );
    glGenTextures( 1, &texture );
    glActiveTexture(GL_TEXTURE0);   // We use texture unit 0 only fixed!
    glBindTexture( GL_TEXTURE_2D, texture );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB, img.XRes(),
                  img.YRes(), 0, GL_RGB, GL_UNSIGNED_BYTE, img.Ptr() );
    glGenerateMipmap( GL_TEXTURE_2D );
}

void Map::setCamera(Camera* camera) {
    glUniform3f(cameraUniform, camera->getX(), camera->getY(), camera->getZ());
    glUniformMatrix4fv(matrixAUniform, 1, GL_FALSE, glm::value_ptr(camera->getAlfaMatrix()));
    glUniformMatrix4fv(matrixBUniform, 1, GL_FALSE, glm::value_ptr(camera->getBetaMatrix()));
    glUniformMatrix4fv(matrixGUniform, 1, GL_FALSE, glm::value_ptr(camera->getGammaMatrix()));
}

void Map::draw() {
    glBindVertexArray(VertexArrayID);
    glBindBuffer(GL_ARRAY_BUFFER,vertexbuffer);

    glActiveTexture(GL_TEXTURE0);
    glUniform1i(textureUniform, 0);  // texture unit 0
    glBindTexture(GL_TEXTURE_2D, texture);

    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
}

Map::~Map() {
    glDeleteBuffers(1, &vertexbuffer);
    glDeleteVertexArrays(1, &VertexArrayID);
    glDeleteProgram(pId);
}