#ifndef TUTORIALS_CAMERA_H
#define TUTORIALS_CAMERA_H
#include <glm/glm.hpp>


class Camera {
public:
    Camera(float x, float y, float z, float ratio);
    void zoomIn();
    void zoomOut();
    void zoomReset();
    void moveHor(int dir);
    void moveVer(int dir);
    void transVer(int dir);
    void transHor(int dir);
    void transRot(int dir);
    void switch3D();

    float getX() const;
    float getY() const;
    float getZ() const;
    bool is3D() const;
    glm::mat4 getAlfaMatrix() const;
    glm::mat4 getBetaMatrix() const;
    glm::mat4 getGammaMatrix() const;

private:
    int alfa, beta, gamma;
    float x,y,z, defZ, ratio, zoomStep, posStep;
    bool mode3D;
};


#endif //TUTORIALS_CAMERA_H
