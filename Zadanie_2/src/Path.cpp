#include "Path.h"

Path::Path(std::vector<GLfloat> vertexes, std::vector<glm::vec3> tracksInfo) {
    this->vertexes = vertexes;
    this->tracksInfo = tracksInfo;
}

void Path::setShaders() {
    pId = LoadShaders("SimpleVertexShader.vertexshader", "SimpleFragmentShader.fragmentshader");
    track_uniform = glGetUniformLocation(pId,"track");
    cameraUniform = glGetUniformLocation(pId,"camera");
    matrixAUniform = glGetUniformLocation(pId, "matrixA");
    matrixBUniform = glGetUniformLocation(pId, "matrixB");
    matrixGUniform = glGetUniformLocation(pId, "matrixG");
}

void Path::setCamera(Camera* camera) {
    glUniform3f(cameraUniform, camera->getX(), camera->getY(), camera->getZ());
    glUniformMatrix4fv(matrixAUniform, 1, GL_FALSE, glm::value_ptr(camera->getAlfaMatrix()));
    glUniformMatrix4fv(matrixBUniform, 1, GL_FALSE, glm::value_ptr(camera->getBetaMatrix()));
    glUniformMatrix4fv(matrixGUniform, 1, GL_FALSE, glm::value_ptr(camera->getGammaMatrix()));
}

void Path::setBuffers(bool mode3D) {
    size_t stride = mode3D ? 0 : sizeof(GLfloat)*6;
    glUseProgram(pId);

    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);
    glGenBuffers(1, &vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER, vertexes.size() * sizeof(GLfloat), &vertexes[0], GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(
            0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
            3,                  // size
            GL_FLOAT,           // type
            GL_FALSE,           // normalized?
            stride,             // stride
            (void *) 0          // array buffer offset
    );
}

void Path::draw(bool mode3D) {

    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glLineWidth(5);
    int offset = 0;
    for(int i = 0; i<tracksInfo.size(); i++){
        glUniform4f(track_uniform, i, tracksInfo.size(), tracksInfo[i].x, tracksInfo[i].y);
        if(!mode3D){
            glDrawArrays(GL_LINE_STRIP, offset, tracksInfo[i].z/2);
        } else{
            glDrawArrays(GL_TRIANGLE_STRIP, offset, tracksInfo[i].z);
        }
        offset += tracksInfo[i].z;
    }
    glDisableVertexAttribArray(0);

}

Path::~Path() {
    glDeleteBuffers(1, &vertexbuffer);
    glDeleteVertexArrays(1, &VertexArrayID);
    glDeleteProgram(pId);
}