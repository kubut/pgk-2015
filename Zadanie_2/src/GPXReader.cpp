#include "GPXReader.h"
#include <string.h>
#include <string>
#include <fstream>

void GPXReader::readGPX(int filesCount, char *filesNames[], float ratioY, float ratioX){
    int i = 3;
    bool init = false;
    bool latlon = false;
    char *line;
    std::string lineStr;

    while(i < filesCount){
        bool initPath = false;
        float minH, maxH;
        int j = 0;
        std::ifstream fileStream(filesNames[i]);
        if(fileStream.is_open()){
            while(getline (fileStream, lineStr)){
                float lat,lon,ele;
                char *p;
                line = const_cast<char*>(lineStr.c_str());
                if (strstr(line, "<trkpt ")){
                    sscanf( strstr(line, "lat=\"")+5, "%f", &lon);
                    sscanf( strstr(line, "lon=\"")+5, "%f", &lat);
                    lon = 50*log(tan((M_PI/4) + (lon*M_PI/360)));
                    lon *= ratioY;
                    lat *= ratioX;
                    latlon = true;
                }
                else if (p=strstr(line, "<ele>")){
                    sscanf( p+5, "%f</ele>", &ele);
                    if (latlon) {
                        latlon = false;
                        if(!initPath){
                            minH = ele;
                            maxH = ele;
                            initPath = true;
                        } else {
                            if (minH > ele) minH = ele;
                            if (maxH < ele) maxH = ele;
                        }
                        if (!init) {
                            init = true;
                            minY = lon;
                            minX = lat;
                            maxY = lon;
                            maxX = lat;
                        } else {
                            if (minY > lon) minY = lon;
                            if (minX > lat) minX = lat;
                            if (maxY < lon) maxY = lon;
                            if (maxX < lat) maxX = lat;
                        }
                        j++;
                        path.push_back(lat);
                        path.push_back(lon);
                        path.push_back(ele);
                        path.push_back(lat);
                        path.push_back(lon);
                        path.push_back(0.0);
                    }
                }
            }
            trackInfo.push_back(glm::vec3(minH, maxH, j*2));
        }
        i++;
    }
}