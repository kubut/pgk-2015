#include "Camera.h"

Camera::Camera(float x, float y, float z, float ratio) {
    this->x = x;
    this->y = y;
    this->z = z;
    this->defZ = z;
    this->alfa = -10;
    this->beta = 0;
    this->gamma = 0;
    this->ratio = ratio;
    this->zoomStep = 1.05;
    this->posStep = 0.05;
    this->mode3D = false;
}

void Camera::zoomIn() {
    z *= zoomStep;
}
void Camera::zoomOut() {
    z /= zoomStep;
}
void Camera::zoomReset() {
    z = defZ;
}
void Camera::moveHor(int dir) {
    x += dir * posStep/z/ratio;
}
void Camera::moveVer(int dir) {
    y += dir * posStep/z/ratio;
}
void Camera::transVer(int dir) {
    alfa += dir;
}
void Camera::transHor(int dir) {
    beta += dir;
}
void Camera::transRot(int dir){
    gamma += dir;
}
void Camera::switch3D() {
    mode3D = !mode3D;
}

float Camera::getZ() const {
    return z-1.0;
}
float Camera::getY() const {
    return y;
}
float Camera::getX() const {
    return x;
}
bool Camera::is3D() const {
    return this->mode3D;
}

glm::mat4 Camera::getAlfaMatrix() const {
    glm::mat4 matrix;
    float radians = alfa*M_PI / 180;

    if(mode3D){
        matrix[0] = glm::vec4(1.0,0.0,0.0,0.0);
        matrix[1] = glm::vec4(0.0,cos(radians),sin(radians),0.0);
        matrix[2] = glm::vec4(0.0,-sin(radians),cos(radians),0.0);
        matrix[3] = glm::vec4(0.0,0.0,0.0,1.0);
    } else {
        matrix[0] = glm::vec4(1.0,0.0,0.0,0.0);
        matrix[1] = glm::vec4(0.0,1.0,0.0,0.0);
        matrix[2] = glm::vec4(0.0,0.0,1.0,0.0);
        matrix[3] = glm::vec4(0.0,0.0,0.0,1.0);
    }

    return matrix;
}

glm::mat4 Camera::getBetaMatrix() const {
    glm::mat4 matrix;
    float radians = beta*M_PI / 180;

    if(mode3D){
        matrix[0] = glm::vec4(cos(radians),0.0,-sin(radians),0.0);
        matrix[1] = glm::vec4(0.0,1.0,0.0,0.0);
        matrix[2] = glm::vec4(sin(radians),0.0, cos(radians),0.0);
        matrix[3] = glm::vec4(0.0,0.0,0.0,1.0);
    } else {
        matrix[0] = glm::vec4(1.0,0.0,0.0,0.0);
        matrix[1] = glm::vec4(0.0,1.0,0.0,0.0);
        matrix[2] = glm::vec4(0.0,0.0,1.0,0.0);
        matrix[3] = glm::vec4(0.0,0.0,0.0,1.0);
    }

    return matrix;
}

glm::mat4 Camera::getGammaMatrix() const {
    glm::mat4 matrix;
    float radians = gamma*M_PI / 180;

    matrix[0] = glm::vec4(cos(radians), -sin(radians),0.0,0.0);
    matrix[1] = glm::vec4(sin(radians), cos(radians),0.0,0.0);
    matrix[2] = glm::vec4(0.0,0.0,1.0,0.0);
    matrix[3] = glm::vec4(0.0,0.0,0.0,1.0);

    return matrix;
}