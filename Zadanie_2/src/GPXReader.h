#ifndef TUTORIALS_GPXREADER_H
#define TUTORIALS_GPXREADER_H

#include <vector>
#include <GL/glew.h>
#include <glm/glm.hpp>

class GPXReader {
public:
    void readGPX(int filesCount, char *filesNames[], float ratioY, float ratioX);

    const std::vector<GLfloat> &getPath() const {
        return path;
    }
    const std::vector<glm::vec3> &getTrackInfo() const {
        return trackInfo;
    }
    const float getWidth() const{
        return std::abs(minX - maxX);
    }
    const float getHeight() const{
        return std::abs(minY - maxY);
    }
    const float getXSum() const{
        return minX + maxX;
    }
    const float getYSum() const{
        return minY + maxY;
    }

private:
    float minY, minX, maxY, maxX;
    std::vector<GLfloat> path;
    std::vector<glm::vec3> trackInfo;
};


#endif //TUTORIALS_GPXREADER_H
