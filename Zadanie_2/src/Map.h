#ifndef TUTORIALS_MAP_H
#define TUTORIALS_MAP_H

#include "MapImage.h"
#include "Camera.h"
#include <GL/glew.h>
#include <glfw3.h>
#include <glm/glm.hpp>
#include <vector>
#include <common/shader.hpp>

class Map {
public:
    Map(char* file, char* map);
    void setTexture();
    void setShaders();
    void setBuffers();
    void setCamera(Camera* camera);
    void draw();
    ~Map();

private:
    MapImage img;
    GLint textureUniform, cameraUniform, matrixAUniform, matrixBUniform, matrixGUniform;
    GLuint pId, VertexArrayID, vertexbuffer, texture;
};


#endif //TUTORIALS_MAP_H
