#include "MapImage.h"

MapImage::MapImage(char* path, char* map) {
    setImage(path);
    setImageMap(map);
}

void MapImage::setImage(char* path) {
    ilInit();

    ILuint ImgId = 0;
    ilGenImages(1, &ImgId);
    ilBindImage(ImgId);

    ilLoadImage(path);

    xsize = ilGetInteger(IL_IMAGE_WIDTH);
    ysize = ilGetInteger(IL_IMAGE_HEIGHT);

    ptr = new AByte[xsize*ysize*3];
    ilCopyPixels(0, 0, 0, xsize, ysize, 1, IL_RGB, IL_UNSIGNED_BYTE, ptr);

    ilBindImage(0);
    ilDeleteImage(ImgId);
}

void MapImage::setImageMap(char *path) {
    std::ifstream fileStream(path);
    std::string lineStr;
    char* line;

    if(fileStream.is_open()){
        while(getline(fileStream, lineStr)){
            line = const_cast<char*>(lineStr.c_str());
            if(strstr(line, "MMPLL, 1, ")){
                sscanf(strstr(line, "MMPLL, 1, ")+10, "%f", &bb[0]);
                sscanf(strstr(line, "MMPLL, 1, ")+23, "%f", &bb[3]);
            } else if(strstr(line, "MMPLL, 3, ")){
                sscanf(strstr(line, "MMPLL, 3, ")+10, "%f", &bb[2]);
                sscanf(strstr(line, "MMPLL, 3, ")+23, "%f", &bb[1]);
            }
        }
    }
}