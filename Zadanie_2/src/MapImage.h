#ifndef TUTORIALS_MAPIMAGE_H
#define TUTORIALS_MAPIMAGE_H

#include <IL/il.h>
#include <fstream>
#include <string.h>

typedef unsigned char AByte;

class MapImage {
public:
    MapImage () {};
    MapImage (char *filename, char *mapName);

    AByte *Ptr ()               { return ptr; };
    inline int XRes () const    { return xsize;  };
    inline int YRes () const    { return ysize;  };
    inline float* getBB()       { return bb; };

    void setImage(char* path);
    void setImageMap(char* path);

private:
    int xsize, ysize;
    float bb[4];
    AByte* ptr;
};


#endif //TUTORIALS_MAPIMAGE_H
