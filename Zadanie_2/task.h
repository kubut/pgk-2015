#ifndef TUTORIALS_TASK_H
#define TUTORIALS_TASK_H

#include <Zadanie_2/src/Camera.h>
#include "src/Map.h"
#include "src/Path.h"
#include <stdio.h>
#include <GL/glew.h>
#include <glfw3.h>
#include <glm/glm.hpp>
#include <vector>
#include "src/GPXReader.h"

GLFWwindow *window;
glm::vec3 windowRatio;
float maxPoint;
int windowWidth = 1000;
int windowHeight = 1000;
Camera* camera;
Path* path;
Map* map;

int initialize();
void mapInit(int filesCount, char *filesNames[]);
void terminate();
void draw();
void readControl();
#endif
