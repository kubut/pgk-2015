#include "task.h"

using namespace glm;

int main(int argc, char *argv[]){
    int code = initialize();
    if(code < 0){
        printf("Error code: %d", code);
        return -1;
    }

    mapInit(argc, argv);
    glClearColor(0.1f, 0.2f, 0.1f, 0.0f);

    draw();

    terminate();
    return 0;
}

void draw(){
    do {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


        map->setBuffers();
        map->setCamera(camera);
        map->draw();

        path->setBuffers(camera->is3D());
        path->setCamera(camera);
        path->draw(camera->is3D());


        glfwSwapBuffers(window);
        glfwPollEvents();

        readControl();
    }
    while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
           glfwWindowShouldClose(window) == 0);
}

void readControl(){
    if(glfwGetKey(window, GLFW_KEY_MINUS) == GLFW_PRESS){
        camera->zoomOut();
    }
    if(glfwGetKey(window, GLFW_KEY_EQUAL) == GLFW_PRESS){
        camera->zoomIn();
    }
    if(glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS){
        camera->zoomReset();
    }
    if(glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS){
        camera->moveHor(1);
    }
    if(glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS){
        camera->moveHor(-1);
    }
    if(glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS){
        camera->moveVer(1);
    }
    if(glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS){
        camera->moveVer(-1);
    }
    if(glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS){
        camera->transVer(1);
    }
    if(glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS){
        camera->transVer(-1);
    }
    if(glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS){
        camera->transHor(-1);
    }
    if(glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS){
        camera->transHor(1);
    }
    if(glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS){
        camera->transRot(-1);
    }
    if(glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS){
        camera->transRot(1);
    }
    if(glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS){
        if(glfwGetKey(window, GLFW_KEY_P) == GLFW_RELEASE) {
            camera->switch3D();
        }
    }
}

int initialize(){
    if (!glfwInit()) {
        return -1; // Failed to initialize GLFW
    }

    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    window = glfwCreateWindow(windowWidth, windowHeight, "Zadanie 2", NULL, NULL);
    if(windowWidth > windowHeight){
        windowRatio = vec3(1.0, windowWidth/windowHeight, windowWidth/windowHeight);
    } else {
        windowRatio = vec3(windowHeight/windowWidth, 1.0, windowHeight/windowWidth);
    }

    if (window == NULL) {
        glfwTerminate();
        return -2; // Failed to open GLFW window
    }

    glfwMakeContextCurrent(window);

    glewExperimental = GL_TRUE ;
    if (glewInit() != GLEW_OK) {
        return -3; // Failed to initialize GLEW
    }

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

    return 0;
}

void mapInit(int filesCount, char *filesNames[]){
    GPXReader reader = GPXReader();
    reader.readGPX(filesCount, filesNames, windowRatio.x, windowRatio.y);

    path = new Path(reader.getPath(), reader.getTrackInfo());
    path->setShaders();
    path->setBuffers(false);

    map = new Map(filesNames[1], filesNames[2]);
    map->setShaders();
    map->setBuffers();
    map->setTexture();

    float width = reader.getWidth();
    float height = reader.getHeight();
    if (width / windowRatio.x > height / windowRatio.y) {
        maxPoint = width;
    } else {
        maxPoint = height;
    }

    camera = new Camera(
            reader.getXSum()/2/windowRatio.x,
            reader.getYSum()/2/windowRatio.y,
            2.0/windowRatio.z/maxPoint,
            windowRatio.z
    );
}

void terminate(){
    delete path;
    delete map;
    delete camera;
    glfwTerminate();
}